Feature: Messages API

    Background:
        Given value message from message.spec
    Scenario: POST message route
        Given Type RequestBody
                | text    | (string) |

        And Type ResponseBody
            | id    | (number) |
            | text | (string)      |

        When POST /messages
        And request-body (RequestBody)
        Then status 201
        And response-body (ResponseBody)
        Examples:
            | text |
            | hello richmond |