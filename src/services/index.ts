import express = require("@feathersjs/express");
import MessageRoute from "./MessageService/message.route";

export default class {
  static init(app: express.Application) {
    MessageRoute.init(app);
  }
}
