// This is the interface for the message data
export interface Message {
  id?: number;
  text: string;
}

// A messages service that allows to create new
// and return all existing messages
export class MessageService {
  private options;
  private app;
  constructor(options, app) {
    this.options = options || {};
    this.app = app;
  }
  messages: Message[] = [];

  async find() {
    // Just return all our messages
    return this.messages;
  }

  async get(id, params) {
    return {
      id, text: `A new message with ID: ${id}!`
    };
  }

  async create(data: Pick<Message, 'text'>) {
    // The new message is the data text with a unique identifier added
    // using the messages length since it changes whenever we add one
    const message: Message = {
      id: this.messages.length,
      text: data.text
    }

    // Add new message to the list
    this.messages.push(message);

    return message;
  }

  async update(id, data, params) {
    return data;
  }

  async patch(id, data, params) {
    return data;
  }

  async remove(id, params) {
    return { id };
  }
}