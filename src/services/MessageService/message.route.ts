import express from "@feathersjs/express";
import { MessageService } from "./message.service";
import hooks from "./message.hooks";

export default class {
  static async init(app: express.Application) {
    const options = {
      paginate: app.get('paginate')
    };
    // Register the message service on the Feathers application
    app.use('messages', new MessageService(options, app));
    
    // Get our initialized service so that we can register hooks
    const service = app.service('messages');
  
    service.hooks(hooks);

  }
}