// Docs - https://docs.feathersjs.com/api/hooks.html
const hook_before_create = (context)=>{
  console.log('BEFORE: hook_before_create', context.data );
  return context;
};
const hook_after_create = (context)=>{
  console.log('AFTER: hook_after_create', context.data );
  return context;
};
const hook_error_create = (context)=>{
  console.log('ERROR: hook_error_create', context.data );
  return context;
};
export default { 
    before: {
      all: [],
      find: [],
      get: [],
      create: [hook_before_create],
      update: [],
      patch: [],
      remove: []
    },

    after: {
      all: [],
      find: [],
      get: [],
      create: [hook_after_create],
      update: [],
      patch: [],
      remove: []
    },

    error: {
      all: [],
      find: [],
      get: [],
      create: [hook_error_create],
      update: [],
      patch: [],
      remove: []
    }
}