import feathers from '@feathersjs/feathers';
import express from '@feathersjs/express';
import '@feathersjs/transport-commons';
import AppInit from "./AppInit";
import appConfig from "./config/appConfig";
console.log(`Starting ${process.env.NODE_ENV || "development"}`);
let app = express(feathers());
AppInit.initializeCors(app);
AppInit.initializeExpressApp(app);
AppInit.initializeServices(app);
AppInit.errorHandler(app);
let port = appConfig.get("port");

app.listen(port, () => {
  console.log("server started at port", port);
  if (process.send) {
    process.send("online");
  }
});
