import express from "@feathersjs/express";
import socketio from '@feathersjs/socketio';
import CorsHelper from "./zlib/Cors";
import AppConfigUtil from "./zlib/AppConfigUtil";
import Services from "./services";

export default class AppInit {
  static initializeCors(app: express.Application) {
    return CorsHelper.initializeCors(app);
  }
  static errorHandler(app: express.Application) {
    app.use((err, req, res, next) => {
      res.send({ error: err.message || err.toString() });
      next();
    });
  }
  static initializeExpressApp(app: express.Application) {
    app.set("trust proxy", true);
    app.use(express.urlencoded({ extended: true }));
    app.use(express.json());
    // Express middleware to to host static files from the current folder
    app.use(express.static(__dirname));
    // Add REST API support
    app.configure(express.rest());
    // Configure Socket.io real-time APIs
    app.configure(socketio());
  }
  static initializeServices(app: express.Application) {
    return Services.init(app);
  }
}
